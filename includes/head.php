<head>
    <title>Questonia - Question of the Day! Increase your knowledge by challenging yourself every day! Win prize!</title>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/screen.css">
    <link rel="stylesheet" href="misc/css/bootstrap.css">
	<link rel="stylesheet" href="misc/css/bootstrap-responsive.css">
	<script type="text/javascript" src="misc/js/bootstrap.js"></script>
	<script type="text/javascript" src="mics/bootstrap/js/bootstrap.js"></script>
    
    <!--<link rel="stylesheet" href="m/css/bootstrap.css">-->
   <meta property="og:url" content="http://www.questonia.com/" />
<meta property="og:site_name" content="Questonia | Question of the Day!" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Questonia.com" />
<meta property="og:image" content="http://www.questonia.com/images/logo2.png" />
<meta property="og:description" content="Question of the Day! Learn everyday by Challenging your self!" />
<meta property="fb:app_id" content="486807608021400" />
   
   
</head>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
                <div class="logo" title="Questonia" ><a  href="/"><img src="../images/logo2.png" width="72" height="72"  border="none"  alt="logo" /></a></div>
                
                