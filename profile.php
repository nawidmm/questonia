<?php 

include 'core/init.php';
include 'includes/overall/overallheader.php';
include 'core/analyticstracking.php'; 
protect_page();

if (isset($_GET['username']) === true && empty($_GET['username']) === false) {
	$username = $_GET['username'];
	
	if (user_exists($username) === true) {
		$user_id = user_id_from_username($username);
		$profile_data = user_data($user_id, 'first_name', 'last_name', 'email');	
	?>
    
    <h1><?php echo $profile_data['first_name']; ?>'s Profile</h1>
    <h2><?php echo 'First Name:&nbsp', $profile_data['first_name']; ?> </h2>
    <h2><?php echo 'Last Name:&nbsp',$profile_data['last_name']; ?> </h2>
    <h2><?php echo 'Your Email:&nbsp',$profile_data['email']; ?> </h2>
    <?php

	} else{
		
		//echo 'Sorry, that user doesn\'t exist!';
	}

	
	
} else {
	header('Location: index.php');
	exit();
	

}

 
  if (logged_in() === true) {
	  include 'includes/widgets/loggedin.php';
	
	}


include 'includes/overall/overallfooter.php'; 
?>

<?php include_once("core/analyticstracking.php") ?>