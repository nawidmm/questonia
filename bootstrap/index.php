<?php 

function echoActiveClassIfRequestMatches($requestUri)
{
    $current_file_name = basename($_SERVER['REQUEST_URI'], ".php");

    if ($current_file_name == $requestUri)
        echo 'class="active"';
}


?>
 <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand" href="#">AuctionBase</a>
            <div class="nav-collapse">
                   <ul class="nav">
    <li <?=echoActiveClassIfRequestMatches("home")?>>
        <a href="home.php">Search</a></li>
    <li <?=echoActiveClassIfRequestMatches("about")?>>
        <a href="about.php">About</a></li>
</ul>
            </div>
        </div>
    </div>
</div>



<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Questonia</title>
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/bootstrap-responsive.css">

	
	</head>
    


	<body>   



		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-th-list"></span>
				</a>
                
             <a href="#" class="brand"><img src="img/logo.png"></a>
				
				<div class="nav-collapse collapse">
					<ul class="nav pull-right">
                    	<li class=" divider-vertical"></li>
						<li class="active"><a href="#" >Home</a></li>
                        <li class=" divider-vertical"></li>
						<li ><a href="/">Q of The Day</a></li>
                        <li class=" divider-vertical"></li>
						<li><a href="#">Prize of The Day</a></li>
                        <li class=" divider-vertical"></li>
						<li style="background-color:#966"><a href="#">Geniusis</a></li>
                        <li class=" divider-vertical"></li>
						<li><a href="#">Explore</a></li>
                        <li class=" divider-vertical"></li>
						<li><a href="#">Fourm</a></li>
                        <li class=" divider-vertical"></li>
					</ul>
				
				</div>
				</div>
			</div>
		</div>
		
		<div class="hero-unit">
			<h1>Welcome to Questonia</h1>
			<p>Question of the Day! Learn by challenging yourself everyday!</p>
			<p><a href="#" class="btn btn-primary btn-large">Get Started</a></p>
		</div>
<ul class="nav">
<li class="active"><a href="#">Home</a></li>
<li><a href="#">Link 2</a></li>
<li><a href="#">Link 3</a></li>
</ul>

<script type="text/javascript">

    function Ctrl($scope, $http, $routeParams, $location, $route) {

    }



    angular.module('BookingFormBuilder', []).
        config(function ($routeProvider, $locationProvider) {
            $routeProvider.
                when('/', { 
                   template: 'I\'m on the home page', 
                   controller: Ctrl, 
                   pageKey: 'HOME' }).
                when('/page1/create', { 
                   template: 'I\'m on page 1 create', 
                   controller: Ctrl, 
                   pageKey: 'CREATE' }).
                when('/page1/edit/:id', { 
                   template: 'I\'m on page 1 edit {id}', 
                   controller: Ctrl, pageKey: 'EDIT' }).
                when('/page1/published/:id', { 
                   template: 'I\'m on page 1 publish {id}', 
                   controller: Ctrl, pageKey: 'PUBLISH' }).
                otherwise({ redirectTo: '/' });

            $locationProvider.hashPrefix("!");
        }).run(function ($rootScope, $http, $route) {

            $rootScope.$on("$routeChangeSuccess", 
                           function (angularEvent, 
                                     currentRoute,
                                     previousRoute) {

                var pageKey = currentRoute.pageKey;
                $(".pagekey").toggleClass("active", false);
                $(".pagekey_" + pageKey).toggleClass("active", true);
            });

        });

</script>
<div class="navbar navbar-inverse">
    <div class="navbar-inner">
        <a class="brand" href="#">Title</a>
        <ul class="nav">
            <li><a href="#!/" class="pagekey pagekey_HOME">Home</a></li>
            <li><a href="#!/page1/create" class="pagekey pagekey_CREATE">Page 1 Create</a></li>
            <li><a href="#!/page1/edit/1" class="pagekey pagekey_EDIT">Page 1 Edit</a></li>
            <li><a href="#!/page1/published/1" class="pagekey pagekey_PUBLISH">Page 1 Published</a></li>
        </ul>
    </div>
</div>        
		<div class="container">
        	<div class="row" id="main-content">
            	<div class="span4" id="sidebar">
			<div class="well">
            <form action="/login.php" class="signup" method="post">
            	<fieldset>
            	<legend>Sign In</legend>
                <input type="text" class="input-block-level" placeholder="Username">
                <input type="password" class="input-block-level" placeholder="Password">
                <label class=" checkbox">
                <input type="checkbox"> Remember Me
                </label>

            	<input type="submit" class="btn btn-info" value="Sing In">
                
            	</fieldset>
            </form>
			</div>
            </div>
            </div>
		</div>
        
        <div class="container">
        	<div class="row" id="main-content">
            	<div class="span4" id="sidebar">
			<div class="well">
            <form action="/register.php" class="signup" method="post">
            	<fieldset>
            	<legend>Sign Up</legend>
                <input type="text" class="input-block-level" placeholder="Username">
                <input type="password" class="input-block-level" placeholder="Password">
                <input type="password" class="input-block-level" placeholder="Retype Password">
                <input type="text" class="input-block-level" placeholder="First Name">
                <input type="text" class="input-block-level" placeholder="Last Name">
                <input type="text" class="input-block-level" placeholder="email">
                <label class="checkbox">
                <p>By clicking the "Sign Up" button, you are agreeing and acknowledging that you have read our <a href="terms.php">Terms</a> and <a href="policy.php">Privacy Policy</a>.</p>
				</label>

            	<input id="signIn" type="submit" class="btn btn-info" value="Sing In">
               
            	</fieldset>
            </form>
			</div>
            </div>
            </div>
		</div>
    
		<script src="http://code.jquery.com/jquery-1.9.0.min.js"></script>
		<script src="js/bootstrap.js"></script>
		<footer class="row"></footer>
	</body>
</html>