
<?php 
?>
<!DOCTYPE html>
<html lang="en">
<head>

<!--[if lt IE 8]>
        <div id="upgrade-browser">
            <div class="body">
                <p class="title">Opps! Looks like you’re using a browser that we no longer support.</p>
                <p>Some of the features on our site may not work. We suggest upgrading your browser to the latest version or switching to a browser that we do support.</p>
                <ul>
                                            <li>
                            <a href="http://www.mozilla.com/firefox/">
                                <img alt="" height="32" width="32" src="/assets/myedu/media/universal/widget/unsupported-browser/graphic-firefox.png" />Firefox</a>
                        </li>
                                            <li>
                            <a href="http://www.google.com/chrome">
                                <img alt="" height="32" width="32" src="/assets/myedu/media/universal/widget/unsupported-browser/graphic-chrome.png" />Chrome</a>
                        </li>
                                            <li>
                            <a href="http://www.apple.com/safari/">
                                <img alt="" height="32" width="32" src="/assets/myedu/media/universal/widget/unsupported-browser/graphic-safari.png" />Safari</a>
                        </li>
                                            <li>
                            <a href="http://www.microsoft.com/windows/Internet-explorer/default.aspx">
                                <img alt="" height="32" width="32" src="/assets/myedu/media/universal/widget/unsupported-browser/graphic-internet-explorer.png" />Internet Explorer</a>
                        </li>
                                    </ul>
            </div>
        </div>
    <![endif]-->
	<meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	
    <meta http-equiv="X-UA-Compatible" content="IE=9,chrome=1">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    
	<link rel="stylesheet" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" href="css/style2.css" />
	<title>VimVision :: A world class web design company! Imagine. Create. Innovate.</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script type="text/javascript">
		$(function(){
			function goToByScroll(id){
				$('html,body').animate({scrollTop: $("#"+id).offset().top},'slow');
			}

			$('#menu li').click(function(){
				var getDiv = $(this).attr("id");
				targetDiv = getDiv.split('_');
				goToByScroll(targetDiv[1]);
				return false;
			});

			$(document).scroll(function() {
				var $this = $(this),
				  scrollTop = $this.scrollTop(),
				  sections = $(this).find('.outer_blocks'),
				  topSection = null,
				  minDist = Infinity;

			  sections.each(function() {

					var top = $(this).offset().top,
					bottom = $(this).height() / 2,
					bottom = top + bottom,

					relativeDistance = Math.min(
					  Math.abs(top - scrollTop),
					  Math.abs(bottom - scrollTop)
					);
				if (relativeDistance < minDist) {
				  minDist = relativeDistance;
				  topSection = $(this).attr("id");
				}
			  });
				var targetdiv = '#menu_' + topSection
				$('#menu li').delay(5000).removeClass('active');
				$(targetdiv).delay(5000).addClass('active');
			});
		});
	</script>
	


</head>
<body>


	<div id="wrapper">
		<div id="header" class="clearfix">
			<div class="container">
				<div id="logo">
					<h1><a  href="/"><img src="/images/vLogov3.png" alt="Logo" width="100px" style="border:none;" /></a></h1>
				</div><!-- end logo -->
				<div id="menu">
					<ul>
						<li id="menu_home"><a href="#home"> Home </a></li>
						<li id="menu_portfolio"><a href="#portfolio"> Portfolio </a></li>
						<li id="menu_services"><a href="#services"> Services </a></li>
						<li id="menu_team"><a href="#team"> Team </a></li>
						<li id="menu_contact"><a href="#contact"> Contact </a></li>
					</ul>
				</div><!-- end menu -->
			</div><!-- end container -->
		</div><!-- end header -->

		<div id="home" class="outer_blocks clearfix">
			<div class="container">
				<div id="home_image">
					<img src="images/ResponsiveHome.png" alt="ResponsiveHome" />
				</div><!-- end home_image -->
				<div id="home_text">
					<h1> We have a history of creating responsive web design</h1>
				</div><!-- end home_text -->
			</div><!-- end container -->
		</div><!-- end home -->

		<div id="portfolio" class="outer_blocks clearfix">
			<div class="container">
				<div class="intro_head">
					<h4><img src="images/icon_plus.png" alt="pulse"/> Portfolio</h4>
				</div><!-- end intro_head -->
	
				<div class="intro_text">
					Here is some of our recent work...
				</div><!-- end intro_text -->
				<div class="item_wrapper">
					<div class="image_holder">
						<a href="http://www.questonia.com">
							<img src="images/profile-7.png" alt="Web Design" />
						</a>
						<h5>Questonia</h5>
						<p>WebDesign</p>
					</div>
	
					<div class="image_holder">
						<a href="http://www.foridahorefugees.org"><img src="images/work3.png" alt="Web Design" /></a>
						<h5>For Idaho Refugees (Non-profit)</h5>
						<p>WebDesign</p>
					</div>	
					
					<div  class="image_holder" >
						<a  href="http://www.ehowguru.com"><img  src="images/work2.png" alt="Photography" /></a>
						<h5>eHowGuru</h5>
						<p>WebDesign</p>
					</div>

				</div><!-- end item_wrapper -->
			</div><!-- end container -->
		</div><!-- end portfolio -->

		<div id="services" class="outer_blocks clearfix">
			<div class="container">
				<div class="intro_head">
					<h4><img src="images/icon_plus.png" alt="pulse"/> Services </h4>
				</div><!-- end intro_head -->
	
				<div class="intro_text">
					A united team with one mission! Providing an integrated solutions. Uniqeness is our goal.
				</div><!-- end intro_text -->
	
				<div class="item_wrapper">
					<div id="services_text">
						<p>Vim Vision is multi-platform, digital, and integrated marketing company. We are an innovative and creative organization that inhabits between the wild creative ideas. We are grazing on the canvases full of colors in full spectrum. We take unique approaches to whatever we are doing. We look at our projects differently. We walk on the pathways’ less walked. Our multi-angle imagination converges to one point and then unfolds in the form of a great concept. We appreciate beauty and ingenuousness. We like purity, originality, and simplicity. We create amazing experiences for your consumers, soundly impressing your brand into your customers’ minds.

</p>
					</div><!-- end services_text -->
	
					<div id="services_list">
						<ul>
							<li><img src="images/IconTool.png" alt="IconTool" /><span>Solid Web Design</span>
								<p>Creating amazing website is our experties. </p>
							</li>
                            <li><img src="images/app-icon3.png" alt="IconTool" /><span>Mobile Applications</span>
								<p>Creating mobile applictions is our favorite projects. </p>
							</li>
							<li><img src="images/IconTesting.png" alt="IconTesting" /><span>A/B Testing</span>
								<p>Improving and modifiyng your current applications is our passion.</p>
							</li>
							<li><img src="images/IconDigitalMedia.png" alt="IconDigitalMedia" /><span>Digital Media</span>
								<p>Creating awesome media is our hobby. </p>
							</li>
						</ul>
					</div><!-- end services_list -->
				</div><!-- end item_wrapper -->
			</div><!-- end container -->
		</div><!-- end services -->

		<div id="team" class="outer_blocks clearfix">
			<div class="container">
				<div class="intro_head">
					<h4><img src="images/icon_plus.png" alt="pulse"/> Team </h4>
				</div><!-- end intro_head -->
	
				<div class="intro_text">
					With a flat structure organization and open business meetings, we work as equals, celebrate as a team, and make fun of each other face to face.
				</div><!-- end intro_text -->
	
				<div class="item_wrapper">
                
                	<div class="image_holder">
						<a href="#"><img src="images/nawid.png" alt="Nawid Mohammad Mousa" /></a>
						<h5>Nawid Mohammad Mousa</h5>
						<p>Founder/Designer/Developer.</p>
	
						<div class="team_social">
							<a href="https://twitter.com/VimVisionLLC"><img src="images/twitter.png" alt="FollowTwitter" /></a>
							<a href="https://www.facebook.com/VimVision"><img src="images/facebook.png" alt="Facebook" /></a>
							<a href="http://dribbble.com/vimvision"><img src="images/dribbble.png" alt="Dribbble" /></a>
                            <a href="https://plus.google.com/106749384350382739485/posts"><img src="images/google-plus.png" alt="google+" /></a>
						</div>
					</div>
                    
                    
					<div class="image_holder">
						<a href="#"><img src="images/blake.jpeg" alt="Blake Dietz" /></a>
						<h5>Blake Dietz</h5>
						<p>Designer/Developer.</p>
	
						<div class="team_social">
							<a href="https://twitter.com/DietzTweetz"><img src="images/twitter.png" alt="FollowTwitter" /></a>
							<a href="https://www.facebook.com/blake.dietz"><img src="images/facebook.png" alt="Facebook" /></a>
							<a href="#"><img src="images/dribbble.png" alt="Dribbble" /></a>
                            <a href="https://plus.google.com/100568566701585139243/posts"><img src="images/google-plus.png" alt="google+" /></a>
						</div>
	
					</div>
	
					
				</div><!-- end item_wrapper -->
			</div><!-- end container -->
		</div><!-- end team -->
		

    
   
        
		<div id="contact" class="outer_blocks clearfix">
        <div class="item_wrapper">
			<div class="container">
				<div class="intro_head">
					<h4><img src="images/icon_plus.png" alt="pulse"/> Contact </h4>
				</div><!-- end intro_head -->
	
				<div class="intro_text">
					Get your dinner ready by contacting us
				</div><!-- end intro_text -->  
               <div class="phone"><h2>Call us: (208) 890-1727</h2></br></br><a href="mailto:hello@vimvision.com"><h2>hello@vimvision.com</h2></a></div>
               
                <!-- begin of social icons -->
                    <iframe  src="http://www.vimvision.com/portfolio/contact.php" style="float:left; position:relative;" width="540px" height="1060px"  frameborder="0" seamless></iframe>
					<div class="social">
                   
						<ul class="tt-wrapper">
							<li><a class="tt-gplus" href="https://plus.google.com/106749384350382739485"><span>Google Plus</span></a></li>
							<li><a class="tt-twitter" href="https://www.twitter.com/vimvisionllc"><span>Twitter</span></a></li>
							<li><a class="tt-dribbble" href="http://dribbble.com/vimvision"><span>Dribbble</span></a></li>
							<li><a class="tt-facebook" href="https://www.facebook.com/VimVision"><span>Facebook</span></a></li>
							<li><a class="tt-linkedin" href="#"><span>LinkedIn</span></a></li>
							
						</ul>
                    </div>
					
			<!-- end of social icons -->

<div class="twitterfeed" style="width:342px;"><a class="twitter-timeline" href="https://twitter.com/VimVisionLLC" data-widget-id="307671914481061888">Tweets by @VimVisionLLC</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

</div>
				</div><!-- end item_wrapper -->
			</div><!-- end container -->
		</div><!-- end contact -->


		<div id="footer">
			<div class="container">
				<img src="/images/vLogov3.png" style="width:100px;" />
				<p> Copyright &copy; 2013. All Rights Reserved </p>
			</div><!-- end container -->
		</div><!-- end footer -->
	</div><!-- end wrapper -->
</body>
</html>


