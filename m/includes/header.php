

<header>
	<!--<div class="logo" > <img src="../images/Q.png" width="72" height="72" alt="logo" /></div> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

<?php include 'includes/navigation.php'; ?>

<meta property="og:url" content="http://www.questonia.com/" />
<meta property="og:site_name" content="Questonia | Question of the Day!" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Questonia.com" />
<meta property="og:image" content="http://www.questonia.com/images/logo2.png" />
<meta property="og:description" content="Question of the Day! Learn everyday by Challenging your self!" />
<meta property="fb:app_id" content="486807608021400" />
     	
        <div class="clear"></div>
</header>

