<?php 
	include 'core/init.php';
	
	logged_in_redirect();

	include 'includes/overall/overallheader.php';
	
	include 'core/analyticstracking.php';

?>
<div class="hero-unit">
<h7>Thank you for using Questonia!</h7>
<h6>You Have Logged Out</h6>
<h8>You have successfully signed out and must <a style="font-weight:bold; text-decoration:none;" href="index.php">sign in</a> again to access Questonia.</h8>
<br />
<br />

<h8>If you have any questions, ideas or suggestions please <a style="font-weight:bold; text-decoration:none;" href="contact.php"> contact us</a>!</h8>

</div>


<?php
include 'includes/overall/overallfooter.php'; 


?>

<?php include_once("core/analyticstracking.php") ?>