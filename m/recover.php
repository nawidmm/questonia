<?php 
include 'core/init.php';
logged_in_redirect();
include 'includes/overall/overallheader.php';
include 'core/analyticstracking.php'; 

?>

<h1>Username/Password recovery</h1>
<?php
if (isset($_GET['success']) === true && empty($_GET['success']) === true) {
?>
	<p>Thanks, please check your email. We've emailed you the instruction.</p>
<?php

} else {
	$mode_allowed = array('username', 'password');
	if (isset($_GET['mode']) === true && in_array($_GET['mode'], $mode_allowed) === true) {
		if (isset($_POST['email']) === true && empty($_POST['email']) === false) {
			if (email_exists($_POST['email']) === true){
				recover($_GET['mode'], $_POST['email']);
				header ('Location: recover.php?success');
				exit();
			} else {
				echo '<p>Oops, We couldn\'t find that email address!</p>';
				
			}
	
		}
	?>
	 <div class="widget">
		<div class="inner"> 
		<form action="" method="post">
		<ul>
        <li style=" font-family: Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold;  margin-bottom:5px; margin-top:50px;">
				Please enter your email address:
         </li>
			<li style=" font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;  margin-bottom:10px;" >
				
				<input type="text" name="email" placeholder="e.g. yourname@mail.com">
			</li>
			
			<li >
				<input  class="bt" type="submit" value="Recover" style="text-align:center;">
			</li>
		</ul>
		
		</form>
	</div>
    </div>
	   
		
	<?php
	} else {
		header('Location: index.php' );
		exit();
		
	}
}
?>





<?php include 'includes/overall/overallfooter.php'; ?>

<?php include_once("core/analyticstracking.php") ?>