<?php
// Setup documnet:
include('config/setup.php');
?>


<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php //echo $page_title; ?> -ATOM - Admin Panel VimVision</title>

<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>

<body>

<div class="wrap_overall">

    <div class="header"> <?php head(); ?> </div>
    
    <div class="nav_main"><?php nav_main(); ?> </div>	
    
    <div class="content"><?php include('content/'.$pg.'.php'); ?> </div>
    
        <div class="footer"><?php footer(); ?> </div>
    
</div>
</body>
</html>