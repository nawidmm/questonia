<?php 
	include 'core/init.php';
	
	logged_in_redirect();

	include 'includes/overall/overallheader.php';
	
	include 'core/analyticstracking.php'; 
	
	
	if (empty($_POST) === false){
		$required_fields = array ('username', 'password', 'password_again', 'first_name', 'email');
		foreach($_POST as $key=>$value){
			if (empty($value) && in_array ($key, $required_fields) === true){
				$errors[] = '<h2> Shucks,... something went wrong! Please fill the form again to register.</h2><br><h4>Feilds marked with an asterisk are required</h4>';
				break 1;
				
			}
			 
		}
	
	if (empty($errors) === true){
		if (user_exists($_POST['username']) === true) {
			$errors[] = '<h4>Sorry, the username \'' . $_POST['username'] . '\' is already taken</h4>';
		}
	if (preg_match("/\\s/",$_POST['username']) == true){
			$errors[] = '<h4>Your username must not contain any spaces</h4>';
		}
		
	if (strlen($_POST['password']) < 8) {
		$errors[] = '<h4>Your password must be at least 8 characters</h4>';
		
		}
		
	if ($_POST['password'] !== $_POST['password_again']){
		$errors[] = '<h4>Your passwords do not match</h4>';
		
		}
		
	if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
		$errors[] = '<h4> A valid email address is required</h4>';
		
		}
	if (email_exists($_POST[email]) === true) {
		$errors[] = '<h4>Sorry, the email  \'' . $_POST['email'] . '\' is already in use</h4>';
		}
		
		
		
	}
		
 }


?>
<div  style="padding-top:200; padding-left:700; ">
<!--<h1>Register</h1>-->

<?php
if (isset($_GET['success']) && empty ($_GET['success'])) {
	echo '<div  style="padding-top:200;  ">You have been registered successfully! Please check your email to activate your account.</div> ';

} else { 
	if (empty($_POST) === false && empty ($errors) === true) {
		$register_data = array(
			'username' 		=> $_POST['username'],
			'password' 		=> $_POST['password'],
			'first_name' 	=> $_POST['first_name'],
			'last_name' 	=> $_POST['last_name'],
			'email' 		=> $_POST['email'],
			'email_code'	=> md5 ($_POST['username'] + microtime())
		
		);
	
		register_user($register_data);
		header('Location: register.php?success');
		exit();
		
	} else if (empty($errors) === false) {
		echo output_errors($errors);
	}

 ?>
 <div class="widget">
<div class="inner">  
<form action="" method="post">

	<ul  id="login">
    
    	<li style=" font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; margin-bottom:10px;">
        	Username*:<br>
            <input type="text" name="username" placeholder="Username">
        </li> 
        
        

         
        <li style=" font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; margin-bottom:10px;">
        First Name*:<br>
        <input type="text" name="first_name" placeholder="First name">
        </li>
        
        <li style=" font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;  margin-bottom:10px;">
      	Last Name:<br>
        <input type="text" name="last_name" placeholder="Last name">
        </li>
        
        <li style=" font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;  margin-bottom:10px;">
      	Email*:<br>
        <input type="text" name="email" placeholder="Email">
        </li>
        
        <li style=" font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;  margin-bottom:10px;">
        	Password*:<br>
            <input type="password" name="password" placeholder="Password">
        </li>
        
        <li style=" font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; margin-bottom:10px;">
        	Password again*:<br>
            <input type="password" name="password_again" placeholder="Retype Password">
        </li>
        <li >
        <input class="bt" type="submit" value="Register">
        </li>
	</ul>
</form>

</div>
</div>

</div>

<?php 
}


include 'includes/overall/overallfooter.php'; ?>
<?php include_once("core/analyticstracking.php") ?>

