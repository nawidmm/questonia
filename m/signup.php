<?php 
	include 'core/init.php';

	logged_in_redirect();

	
	
	include 'core/analyticstracking.php'; 
	
	
	if (empty($_POST) === false){
		$required_fields = array ('username', 'password', 'password_again', 'first_name', 'email');
		foreach($_POST as $key=>$value){
			if (empty($value) && in_array ($key, $required_fields) === true){
				$errors[] = '<h2> Shucks,... something went wrong! Please fill the form again to register.</h2><br><h4>Feilds marked with an asterisk are required</h4>';
				break 1;
				
			}
			 
		}
	
	if (empty($errors) === true){
		if (user_exists($_POST['username']) === true) {
			$errors[] = '<h4>Sorry, the username \'' . $_POST['username'] . '\' is already taken</h4>';
		}
	if (preg_match("/\\s/",$_POST['username']) == true){
			$errors[] = '<h4>Your username must not contain any spaces</h4>';
		}
		
	if (strlen($_POST['password']) < 8) {
		$errors[] = '<h4>Your password must be at least 8 characters</h4>';
		
		}
		
	if ($_POST['password'] !== $_POST['password_again']){
		$errors[] = '<h4>Your passwords do not match</h4>';
		
		}
		
	if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
		$errors[] = '<h4> A valid email address is required</h4>';
		
		}
	if (email_exists($_POST[email]) === true) {
		$errors[] = '<h4>Sorry, the email  \'' . $_POST['email'] . '\' is already in use</h4>';
		}
		
		
		
	}
		
 }


?>



<?php
if (isset($_GET['success']) && empty ($_GET['success'])) {
	echo 'You have been registered successfully! Please check your email to activate your account. ';

} else { 
	if (empty($_POST) === false && empty ($errors) === true) {
		$register_data = array(
			'username' 		=> $_POST['username'],
			'password' 		=> $_POST['password'],
			'first_name' 	=> $_POST['first_name'],
			'last_name' 	=> $_POST['last_name'],
			'email' 		=> $_POST['email'],
			'email_code'	=> md5 ($_POST['username'] + microtime())
		
		);
	
		register_user($register_data);
		header('Location: register.php?success');
		exit();
		
	} else if (empty($errors) === false) {
		echo output_errors($errors);
	}

?>















<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Questonia &middot; Mobile Site &middot; Create an account</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="../assets/ico/favicon.png">
  </head>

  <body>
<img  style=" padding-bottom:20px; padding-top:20px; width:140; height:140; display:block; margin-left:auto; margin-right:auto; " src="img/q-logo.png" class="img-rounded">
   
<div class="container">
        <!-- <div class="well">
            <a href="#" id="example" class="btn btn-success" rel="popover" data-content="It's so simple to create a tooltop for my website!" data-original-title="Twitter Bootstrap Popover">hover for popover</a>
         </div>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    
      <script>
      $(function (){
         $("#example").popover({placement:'bottom'});
      });
      </script>-->
      <form  id="login"  method="post" class="form-signin">
        <h3 class="form-signin-heading">Create an account</h3>
        <input name="username" type="text" class="input-block-level" placeholder="Username">
        <input name="first_name" type="text" class="input-block-level" placeholder="First Name">
        <input name="last_name" type="text" class="input-block-level" placeholder="Last Name">
        <input name="email" type="text" class="input-block-level" placeholder="Email address">
        <input name="password" type="password" class="input-block-level" placeholder="Password">
        <input name="password_again" type="password" class="input-block-level" placeholder="Password again">
        <button class="btn btn-large btn-primary" value="Register" type="submit">Sign up</button>
       
     </form>
     
     
<div class="form-signin"> 
<h5 style=" display:block; margin-left:auto; margin-right:auto; padding-bottom:15px;" class="form-signin-heading">Already have an account?</h5>
<a style=" display:block; margin-left:auto; margin-right:auto; text-align: center;" href="/m/index.php" role="button" class="btn-large btn-success " data-toggle="modal">Back to Login</a>
 
<!-- Modal -->
<!--<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Modal header</h3>
  </div>
  <div class="modal-body">
    <form class="form-signin">
        <h2 class="form-signin-heading">Please sign up</h2>
        <input type="text" class="input-block-level" placeholder="User Name">
        <input type="text" class="input-block-level" placeholder="First Name">
        <input type="text" class="input-block-level" placeholder="Last Name">
        <input type="text" class="input-block-level" placeholder="Email address">
        <input type="password" class="input-block-level" placeholder="Password">
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
        <button class="btn btn-large btn-primary" type="submit">Sign in</button>
       </br>
       </br>
       </br>
      </form>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <button class="btn btn-primary">Save changes</button>
  </div>
</div>
</div><!-- end Sign UP buttob-->
</div>
</div> <!-- /container -->




 <!-- Button to trigger modal -->

      
  
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap-transition.js"></script>
    <script src="js/bootstrap-alert.js"></script>
    <script src="js/bootstrap-modal.js"></script>
    <script src="js/bootstrap-dropdown.js"></script>
    <script src="js/bootstrap-scrollspy.js"></script>
    <script src="js/bootstrap-tab.js"></script>
    <script src="js/bootstrap-tooltip.js"></script>
    <script src="js/bootstrap-popover.js"></script>
    <script src="js/bootstrap-button.js"></script>
    <script src="js/bootstrap-collapse.js"></script>
    <script src="js/bootstrap-carousel.js"></script>
    <script src="js/bootstrap-typeahead.js"></script>


<?php 
}

?>
<?php include_once("core/analyticstracking.php") ?>  

</body>
</html>