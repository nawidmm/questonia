<?php

include 'core/init.php';

include 'includes/overall/headerx.php';
include 'core/analyticstracking.php';


?>

<h4>Privacy</h4>



<div style=" page:auto; text-align:justify;"  class="small">
<div class="WordSection1">

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:16.5pt"><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">This privacy policy sets out how “Questonia.com” uses and protects any information that you give
“Questonia.com” when you use this website. <br>
“Questonia.com” is committed to ensuring that your privacy is protected. Should
we ask you to provide certain information by which you can be identified when
using this website, then you can be assured that it will only be used in
accordance with this privacy statement.<br>
“Questonia.com” may change this policy from time to time by updating this page.
You should check this page from time to time to ensure that you are happy with
any changes. This policy is effective from 8/26/2013.<br>
<br>
<b>What we collect</b>&nbsp;<br>
<span class="GramE">We</span> may collect the following information:<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:.25in;text-indent:-.25in;line-height:16.5pt;mso-list:l1 level1 lfo1;
tab-stops:list .5in"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#222222"><span style="mso-list:Ignore">·<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Name <o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:.25in;text-indent:-.25in;line-height:16.5pt;mso-list:l1 level1 lfo1;
tab-stops:list .5in"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#222222"><span style="mso-list:Ignore">·<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Address<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:.25in;text-indent:-.25in;line-height:16.5pt;mso-list:l1 level1 lfo1;
tab-stops:list .5in"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#222222"><span style="mso-list:Ignore">·<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Contact information
including email address<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:.25in;text-indent:-.25in;line-height:16.5pt;mso-list:l1 level1 lfo1;
tab-stops:list .5in"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#222222"><span style="mso-list:Ignore">·<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Demographic
information such as postcode, preferences and interests<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:.25in;text-indent:-.25in;line-height:16.5pt;mso-list:l1 level1 lfo1;
tab-stops:list .5in"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#222222"><span style="mso-list:Ignore">·<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Other information
relevant to customer surveys and/or offers<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:16.5pt"><b><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">What we do with the
information we gather</span></b><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222"><br>
<span class="GramE">We</span> require this information to understand your needs
and provide you with a better service, and in particular for the following
reasons:<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:.25in;text-indent:-.25in;line-height:16.5pt;mso-list:l0 level1 lfo2;
tab-stops:list .5in"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#222222"><span style="mso-list:Ignore">·<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Internal record
keeping.<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:.25in;text-indent:-.25in;line-height:16.5pt;mso-list:l0 level1 lfo2;
tab-stops:list .5in"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#222222"><span style="mso-list:Ignore">·<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">We may use the
information to improve our products and services.<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:.25in;text-indent:-.25in;line-height:16.5pt;mso-list:l0 level1 lfo2;
tab-stops:list .5in"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#222222"><span style="mso-list:Ignore">·<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">We may periodically
send promotional email about new products, special offers or other information
which we think you may find interesting using the email address which you have
provided.&nbsp;<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:.25in;text-indent:-.25in;line-height:16.5pt;mso-list:l0 level1 lfo2;
tab-stops:list .5in"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#222222"><span style="mso-list:Ignore">·<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">From time to time, we
may also use your information to contact you for market research purposes. We
may contact you by email, phone, fax or mail.<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:.25in;text-indent:-.25in;line-height:16.5pt;mso-list:l0 level1 lfo2;
tab-stops:list .5in"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#222222"><span style="mso-list:Ignore">·<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">We may use the
information to customize the website according to your interests.<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:.25in;text-indent:-.25in;line-height:16.5pt;mso-list:l0 level1 lfo2;
tab-stops:list .5in"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#222222"><span style="mso-list:Ignore">·<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">We may provide your
information to our third party partners for marketing or promotional purposes.<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
margin-left:.25in;text-indent:-.25in;line-height:16.5pt;mso-list:l0 level1 lfo2;
tab-stops:list .5in"><!--[if !supportLists]--><span style="font-size:10.0pt;
mso-bidi-font-size:12.0pt;font-family:Symbol;mso-fareast-font-family:Symbol;
mso-bidi-font-family:Symbol;color:#222222"><span style="mso-list:Ignore">·<span style="font:7.0pt &quot;Times New Roman&quot;">&nbsp;&nbsp;&nbsp;
</span></span></span><!--[endif]--><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">We will never sell
your information.<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:16.5pt"><b><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">Security</span></b><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;color:#222222">&nbsp;<br>
We are committed to ensuring that your information is secure. In order to
prevent unauthorized access or disclosure we have put in place suitable
physical, electronic and managerial procedures to safeguard and secure the
information we collect online.<o:p></o:p></span></p>

<p class="MsoNormal" style="mso-margin-top-alt:auto;mso-margin-bottom-alt:auto;
line-height:16.5pt"><b><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
mso-fareast-font-family:&quot;Times New Roman&quot;;color:#222222">How we use cookies</span></b><span style="font-size:12.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;color:#222222">&nbsp;<br>
<span class="GramE">A</span> cookie is a small file which asks permission to be
placed on your computer's hard drive. Once you agree, the file is added and the
cookie helps analyze web traffic or lets you know when you visit a particular
site. Cookies allow web applications to respond to you as an individual. The
web application can tailor its operations to your needs, likes and dislikes by
gathering and remembering information about your preferences.&nbsp;<br>
<br>
We use traffic log cookies to identify which pages are being used. This helps
us analyze data about web page traffic and improve our website in order to
tailor it to customer needs. We only use this information for statistical
analysis purposes and then the data is removed from the system.&nbsp;<br>
<br>
Overall, cookies help us provide you with a better website, by enabling us to
monitor which pages you find useful and which you do not. A cookie in no way
gives us access to your computer or any information about you, other than the
data you choose to share with us.&nbsp;<br>
<br>
You can choose to accept or decline cookies. Most web browsers automatically
accept cookies, but you can usually modify your browser setting to decline
cookies if you prefer. This may prevent you from taking full advantage of the
website.<br>
<br>
<b>Links to other websites</b><br>
<span class="GramE">Our</span> website may contain links to enable you to visit
other websites of interest easily. However, once you have used these links to
leave our site, you should note that we do not have any control over that other
website. Therefore, we cannot be responsible for the protection and privacy of
any information which you provide whilst visiting such sites and such sites are
not governed by this privacy statement. You should exercise caution and look at
the privacy statement applicable to the website in question.<o:p></o:p></span></p>

<p class="MsoNormal"><o:p>&nbsp;</o:p></p>

</div>

</div>




 























<?php include 'includes/overall/overallfooter.php'; ?>

<?php include_once("core/analyticstracking.php") ?>