<?php


include 'core/init.php';
protect_page();

if (empty($_POST) === false) {
	$required_fields = array ('current_password', 'password', 'password_again');
	foreach($_POST as $key=>$value){
			if (empty($value) && in_array ($key, $required_fields) === true){
				$errors[] = '<h4>Feilds marked with an asterisk are required</h4>';
				break 1;
				
			}
			 
		}
	
	if (md5($_POST['current_password']) === $user_data['password']) {
		if (trim($_POST['password']) !== trim($_POST['password_again'])) {
			$errors[] = 'Your new passwords do not much';	
		} else if (strlen($_POST['password'])< 8) {
			$errors[] = '<h4>Your new password must be at least 8 characters</h4>';
		
		}
		
	} else {
		$errors[] = '<h4>Your current password is incorrect</h4>';
	
	}

}

include 'includes/overall/overallheader.php'; 

?>

<h1>Change Password</h1>

<?php

if (isset($_GET['success']) === true && empty ($_GET['success']) === true) {
	echo' <h4>You password has been changed.</h4>';

} else { 

	if(isset($_GET['force']) === true && empty ($_GET['force']) === true){
?>
<p>Per your request, now you must change your password. </p>
<?php
	
	
}
if (empty($_POST) === false && empty ($errors) === true) {
	change_password($session_user_id, $_POST['password']);
	header('Location: changepassword.php?success');
} else if (empty($errors) === false){
	echo output_errors($errors);

}
?>
 <div class="setting">
<form action="" method="post">
	<ul>
    	<li style=" font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; margin-bottom:10px;">
        	Current password*:<br>
            <input type="password" name="current_password" placeholder="Current Password">
		</li>
        
        <li style=" font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; margin-bottom:10px;">
        	New password*:<br>
            <input type="password" name="password" placeholder="New Password">
		</li>
        
        <li style=" font-family: Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; margin-bottom:10px;">
        	New password again*:<br>
            <input type="password" name="password_again" placeholder="Retype new Password">
		</li>
        
        <li>
        	<input class="bts"  type="submit" value="Change Password">
        </li>

</ul>
</form>
<div>

<?php 
}
include 'includes/overall/overallfooter.php'; 
?>



